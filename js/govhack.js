$(document).ready(function(){
   
   var chart = c3.generate({
      bindto: '#chart',
      size: {
         height: 600
      },
      data: {
         type: 'bar',
         columns: [nswFemaleIncome, nswMaleIncome],
         colors: {
            'NSW (Female)': femaleColours[0],
            'NSW (Male)': maleColours[0]
         }
      },
      axis: {
         // rotated: true,
         x: {
            label: {
               text:'Age',
               position: 'outer-right'
            },
            type:'category',
            categories: ['18-24', '25-29', '30-34', '35-39', '40-44', '45-49', '50-54', '55-59', '60-64', '65-69', '70-74', '75 & over']
         },
         y: {
            label: {
               text:'Average Taxable Income',
               position: 'outer-middle'
            },
            tick: {
               format: d3.format('$,')
            }
         }
      }
   });

   var getCheckedBoxes = function(panelId) {
      return $.map($(panelId).find('input:checked'), function(elt, index){return $(elt).attr('id')});
   };

   var smartColours = function(dataNames, n){
      var tailoredColours = {};
      $.each(dataNames, function(i, name) {
         var nth = Math.floor(i / n);
         var factor = Math.floor(nth / 2) / 2; //div again because we ahve 2 colours


         if (name.indexOf('Total') !== -1) {
            // Total colour
            tailoredColours[name] = d3.rgb(totalColours[nth % 2]).darker(factor);

         } else if (name.indexOf('Female') !== -1) {
            // Female colour
            tailoredColours[name] = d3.rgb(femaleColours[nth % 2]).darker(factor);
         } else {
            // Male colour
            tailoredColours[name] = d3.rgb(maleColours[nth % 2]).brighter(factor);
         }
         console.log("Darkening: '" + name + "'' by: " + factor)
      });

      return tailoredColours;
   };

   var getNewDataSet = function() {
      var states = getCheckedBoxes('#state-controls');
      var genders = getCheckedBoxes('#gender-controls');
      var result = [];
      var requiredData = [];
      var dataNames = [];
      var tailoredColours = {};
      $.each(states, function(i, state){
         $.each(genders, function(j, gender){
            result.push(state + '-' + gender);            
         });
      });

      $.each(result, function(i, key){
         requiredData.push(dataMapping[key]);
         dataNames.push(dataMapping[key][0]);
      });

      tailoredColours = smartColours(dataNames, genders.length);

      // unload existing data
      chart.unload();
      setTimeout(function(){
         chart.load({columns: requiredData});
         chart.data.colors(tailoredColours);
      }, 350); // seems like we need a timeout otherwise c3js goes crazy
      
   };

   // Handle all the things!
   $('.state-trigger, .gender-trigger').change(function(e) {    
      getNewDataSet();
   });
});
