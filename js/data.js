// Glorius data!
// All as arrays as it's easier to handle with c3js - data munging FTW!
var nswFemaleIncome = ['NSW (Female)',34260,48371,56591,60899,62709,61470,59561,60250,55351,65914,71182,81973];
var vicFemaleIncome = ['VIC (Female)',34160,46385,53174,56251,57732,57596,55823,56945,53438,63614,72011,80738];
var qldFemaleIncome = ['QLD (Female)',35084,47313,52034,54652,56003,55293,54674,56312,51840,58599,65202,72292];
var waFemaleIncome = ['WA (Female)',38347,52601,58412,60398,61734,62548,61906,62422,57629,66791,72093,73317];
var saFemaleIncome = ['SA (Female)',33886,45529,49674,50890,52915,53135,53548,54861,51896,60608,63580,71435];
var tasFemaleIncome = ['TAS (Female)',32626,44494,46630,47490,48898,50360,50408,51940,49181,56467,60497,64741];
var actFemaleIncome = ['ACT (Female)',38744,56797,64083,67975,72428,76240,74973,71418,61270,59493,54653,68615];
var ntFemaleIncome = ['NT (Female)',38904,49723,55350,60424,61367,63641,64317,65506,62315,62341,61993,73486];
var ausFemaleIncome =  ['Australia (Female)', 35019, 48085, 54556, 57623, 59145, 58797, 57564, 58549, 54240, 63348, 69133, 77701];

var nswMaleIncome = ['NSW (Male)',38514,55131,69652,83604,92156,94017,91365,87451,78121,83852,83699,86142];
var vicMaleIncome = ['VIC (Male)',37652,52505,65131,76870,84377,86551,85737,83868,74234,77369,88309,82948];
var qldMaleIncome = ['QLD (Male)',41969,58574,68837,77991,83755,85432,84234,84163,73178,74295,74664,76224];
var waMaleIncome = ['WA (Male)',48138,69064,82886,95232,103647,106780,105759,102299,89068,85541,92910,77236];
var saMaleIncome = ['SA (Male)',38766,52945,61919,69542,74543,77318,76141,76253,68265,73018,66443,70147];
var tasMaleIncome = ['TAS (Male)',36810,50556,58888,64830,68778,70557,70006,70148,64137,67635,63118,63592];
var actMaleIncome = ['ACT (Male)',39661,59901,74740,86133,94545,100972,100098,99235,84022,76685,62794,68230];
var ntMaleIncome = ['NT (Male)',47129,59541,68809,76399,83967,87489,84902,85358,83933,76208,68424,66884];
var ausMaleIncome =  ['Australia (Male)', 40448, 56856, 69327, 80794, 88055, 90243, 88632, 86523, 76543, 79219, 81534, 80142];

var nswTotalIncome =  ['NSW (Total)', 36547, 52033, 63972, 73773, 78801, 78734, 76223, 74797, 68200, 76622, 78179, 83916];
var vicTotalIncome =  ['VIC (Total)', 36015, 49680, 59907, 68050, 72365, 72952, 71500, 71386, 65288, 72031, 81286, 81762]
var qldTotalIncome =  ['QLD (Total)', 38785, 53555, 61684, 67952, 71203, 71224, 70138, 71405, 64243, 68411, 70740, 74200];
var waTotalIncome =  ['WA (Total)', 43707, 62072, 73093, 81203, 85729, 86757, 85486, 84320, 76136, 78627, 84553, 75290];
var saTotalIncome =  ['SA (Total)', 36526, 49607, 56686, 61491, 64701, 65882, 65232, 66200, 61221, 68379, 65307, 70783];
var tasTotalIncome =  ['TAS (Total)', 34943, 47884, 53622, 57175, 59592, 60738, 60459, 61640, 57944, 63450, 62071, 64154];
var actTotalIncome =  ['ACT (Total)', 39218, 58404, 69719, 77712, 83975, 88547, 87471, 85487, 73581, 69987, 59609, 68416];
var ntTotalIncome =  ['NT (Total)', 43558, 55149, 62758, 69378, 73618, 76279, 75308, 76237, 75066, 71051, 66115, 69683];
var ausTotalIncome =  ['Australia (Total)', 37939, 52890, 62975, 70864, 75032, 75486, 73848, 73579, 67006, 73082, 76290, 78869];

var dataMapping = {
  'nsw-female' : nswFemaleIncome,
  'qld-female' : qldFemaleIncome,
  'vic-female' : vicFemaleIncome,
  'act-female' : actFemaleIncome,
   'nt-female' : ntFemaleIncome,
   'wa-female' :  waFemaleIncome,
   'sa-female' :  saFemaleIncome,
   'aus-female' :  ausFemaleIncome,

  'nsw-male' : nswMaleIncome,
  'qld-male' : qldMaleIncome,
  'vic-male' : vicMaleIncome,
  'act-male' : actMaleIncome,
   'nt-male' :  ntMaleIncome,
   'wa-male' :  waMaleIncome,
   'sa-male' :  saMaleIncome,
  'aus-male' :  ausMaleIncome,

  'nsw-total' : nswTotalIncome,
  'qld-total' : qldTotalIncome,
  'vic-total' : vicTotalIncome,
  'act-total' : actTotalIncome,
   'nt-total' :  ntTotalIncome,
   'wa-total' :  waTotalIncome,
   'sa-total' :  saTotalIncome,
  'aus-total' :  ausTotalIncome
};

var maleColours = ['#d84315', '#4e342e']; // orange/brown-ish
var femaleColours = ['#259b24', '#cddc39']; // greens
var totalColours = ['#3f51b5', '#9c27b0']; // purple